extends Node

onready var GUI_CONTAINER = get_tree().get_current_scene().get_node("GUIContainer")


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func showDroneSelected(drone):
	self.GUI_CONTAINER.get_node("DroneSelected").openObject(drone)
	
func hideDroneSelected():
	GUI_CONTAINER.get_node("DroneSelected").closeObject()
	
func showAddingPointsMenu(drone):
	self.GUI_CONTAINER.get_node("AddingPointMenu").openObject(drone)
	
func hideAddingPointsMenu():
	GUI_CONTAINER.get_node("AddingPointMenu").closeObject()
	
func hideBottomMenu():
	GUI_CONTAINER.get_node("MainMenu").visible = false
	
func showBottomMenu():
	GUI_CONTAINER.get_node("MainMenu").visible = true
	
	
