extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var bar = $Viewport/ProgressBar

export var max_value = 10
export var value = 10
# Called when the node enters the scene tree for the first time.
func _ready():
	var fgStyle = StyleBoxFlat.new()
	fgStyle.anti_aliasing = false;
	fgStyle.border_width_bottom = 2
	fgStyle.border_width_left = 2;
	fgStyle.border_width_right = 2;
	fgStyle.border_width_top = 2;
	fgStyle.border_color = Color("cccccc")
	fgStyle.corner_radius_bottom_left = 6;
	fgStyle.corner_radius_bottom_right = 6;
	fgStyle.corner_radius_top_left = 6;
	fgStyle.corner_radius_top_right = 6;
	bar.add_stylebox_override("fg",fgStyle)
	
	bar.set_max(max_value)
	bar.set_value(value)
	pass # Replace with function body.

func set_bar_max_value(val):
	max_value = val
	bar.set_max(max_value)
	pass
func set_bar_value(val):
	#lifeBar.max_value = max_health
	value = val
	
	update_bar_render()
	pass
	
func update_bar_render():
	var percent =int(((float(value) / float(max_value)) * 100))
	bar.set_value(value)
	var r = range_lerp(percent, 10, 100, 1, 0)
	var g = range_lerp(percent, 10, 100, 0, 1)
	#print(r, g)
	bar.get("custom_styles/fg").set_bg_color(Color(r, g, 0))

	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

