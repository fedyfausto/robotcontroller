extends KinematicBody


export var speed = 1
export var gravity = -5
export var max_health = 10
export var health = 10
export var power = 1

var target = null
var enemy_target = null
var velocity = Vector3.ZERO
enum STATES  {
	IDLE,
	WALKING,
	WORKING,
	APPROACHING,
	ATTACKING
}
var busy = false

var state

onready var animator = $AnimationPlayer
onready var attackTimer = $AttackTimer
onready var lifeBar = $LifeBar
onready var playersContainer = get_parent()

# Called when the node enters the scene tree for the first time.
func _ready():
	(animator as AnimationPlayer).play("idle");
	lifeBar.set_bar_max_value(max_health)
	lifeBar.set_bar_value(health)
	state = STATES.IDLE
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if !enemy_target:
		var enemy = playersContainer.get_children()[ randi() % playersContainer.get_child_count()]
		if enemy != self and !enemy.busy:
			busy = true
			enemy.busy = true
			enemy_target = enemy
			enemy.enemy_target = self
			state = STATES.APPROACHING
			enemy.state = STATES.APPROACHING
	elif enemy_target and state == STATES.APPROACHING :
		target = enemy_target.transform.origin
		enemy_target.target = transform.origin
	self.update_life_bar()
	#_update_animation();
	pass

func update_life_bar():
	lifeBar.set_bar_value(health)
	pass
	
func go_to_point(delta, point):
	target = point
	pass
	
func _physics_process(delta):
	velocity.y += gravity * delta
	if target:
		state = STATES.WALKING
		if enemy_target:
			state = STATES.APPROACHING
		#(animator as AnimationPlayer).current_animation = "walking";
		look_at(target, Vector3.UP)
		rotation.x = 0
		velocity = -transform.basis.z * speed
		(animator as AnimationPlayer).play("walking");
		if transform.origin.distance_to(target) < .6:
			(animator as AnimationPlayer).play("idle");
			if state == STATES.APPROACHING:
				state = STATES.ATTACKING
				attackTimer.start(1)
			else :
				state = STATES.IDLE
			
			#(animator as AnimationPlayer).current_animation = "idle";
			target = null
			velocity = Vector3.ZERO
	velocity = move_and_slide(velocity, Vector3.UP)

func _update_animation():
	if velocity != Vector3.ZERO :
		(animator as AnimationPlayer).play("walking");
	else:
		(animator as AnimationPlayer).play("idle");
	pass
func _on_AttackTimer_timeout():
	if state == STATES.ATTACKING and enemy_target:
		var attack_result = rand_range(0,1)
		if attack_result > 0.5:
			#print(self.name+" attacca "+enemy_target.name)
			enemy_target.health-= power
			if enemy_target.health <= 0:
				#print(enemy_target.name+" morto")
				enemy_target.queue_free()
				state = STATES.IDLE
				busy = false
				enemy_target = null
		#else :
			#print(self.name+ " missa!")
		
		attackTimer.start(1)	
	pass # Replace with function body.
