extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var floorGame = get_parent().get_node("Environments/Floor/MeshInstance")
onready var objectsContainer = get_parent().get_node("Objects")
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_BtnNewKight_pressed():
	
	var floorTranslation = floorGame.get_scale()
	var rand = randi() % 2
	#var knight = load("res://jam_test/objects/Knight.tscn").instance()
	var knight
	if rand == 0:
		knight = load("res://jam_test/objects/KnightBlue.tscn").instance()
	else:
		knight = load("res://jam_test/objects/KnightRed.tscn").instance()
	
	var place_position = Vector3()
	var posX = rand_range(
		(-floorTranslation.x*.5) + knight.get_scale().x*.5,
		(floorTranslation.x*.5) - knight.get_scale().x*.5
	)
	var posZ = rand_range(
		(- floorTranslation.z*.5) + knight.get_scale().z*.5,
		(floorTranslation.z*.5) - knight.get_scale().z*.5
	)

	place_position.x = posX
	place_position.z = posZ
	place_position.y = knight.get_scale().y - (knight.get_scale().y * .5)
	knight.set_translation(place_position)
	objectsContainer.add_child(knight)
	pass # Replace with function body.
