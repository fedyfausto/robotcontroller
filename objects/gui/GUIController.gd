extends Node

onready var BackgroundDrop = get_parent().get_node("BackgroundDrop");


signal new_object_place

# Called when the node enters the scene tree for the first time.
func _ready():
	BackgroundDrop.visible = false;
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _showBackgraoundDrop ():
	BackgroundDrop.visible = true
	pass
func _hideBackgraoundDrop ():
	BackgroundDrop.visible = false
	pass
func _toggleBackgraoundDrop ():
	BackgroundDrop.visible = !BackgroundDrop.visible
	pass

func _on_BtnNewRobot_pressed():
	#_showBackgraoundDrop();
	var newRobotWindow = load("res://objects/gui/NewRobotPanel.tscn").instance()
	self.add_child(newRobotWindow)
	pass # Replace with function body.


func _on_BtnNewBox_pressed():
	
	var boxScene = load("res://objects/scene_objects/Box.tscn")
	emit_signal("new_object_place", boxScene)

	pass # Replace with function body.


func _on_BtnNewSphere_pressed():
	var sphereScene = load("res://objects/scene_objects/Sphere.tscn")
	emit_signal("new_object_place", sphereScene)
	pass # Replace with function body.
