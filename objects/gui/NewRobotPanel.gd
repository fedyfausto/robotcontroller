extends Control

signal connectionError
onready var BackdropGui = $BackgroundDrop
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_BtnCancel_pressed():
	queue_free()
	pass # Replace with function body.


func _on_BtnConfirm_pressed():
	#self.visible = false
	#var progress = load("res://objects/gui/LoadingView.tscn").instance();
#	progress.setLabel("Connecting to the Robot...");
#	get_parent().add_child(progress)
	var ip = $PanelContainer/VBoxContainer/IP.text
	var port = $PanelContainer/VBoxContainer/Port.text
	var path = $PanelContainer/VBoxContainer/Path.text
	
	
	
	
	var robotInstance = load("res://objects/robot/Robot.tscn").instance()
	get_tree().get_root().get_node("World/Objects").add_child(robotInstance)
	robotInstance.global_transform.origin.y = 0.009
	robotInstance.connect_to_robot(ip, port, path)
	
	
	
	queue_free()
	pass # Replace with function body.
