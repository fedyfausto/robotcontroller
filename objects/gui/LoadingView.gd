extends Control


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var label = $PanelContainer/VBoxContainer/Label
onready var progressbar = $PanelContainer/VBoxContainer/ProgressBar

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func setLabel(string):
	$PanelContainer/VBoxContainer/Label.text = string
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	progressbar.value += 1
	if progressbar.value > 100:
		progressbar.value = 0;
	pass
