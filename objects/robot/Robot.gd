extends KinematicBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var meshSmall = $Small
onready var batteryBar = $BatteryBar
onready var timerCollision = $CollisionEnablerTimer
onready var outline = $RobotSmall/Outline
var scalePosizion = 1000

var rotationAngle
var positionX = null
var positionY = null

var offsetX = 0
var offsetY = 0

#var velocity = Vector3()

var _socket = WebSocketClient.new()
# Called when the node enters the scene tree for the first time.
func _ready():
	_socket.verify_ssl = false
	meshSmall.disabled = true
	batteryBar.set_bar_value(100)
	self.rotationAngle = self.rotation.y
	self.positionX = global_transform.origin.x
	self.positionY = global_transform.origin.z


func _process(delta):
	#set_battery_percent(batteryBar.value-0.1)
	if _socket:
		_socket.poll()
		
func _physics_process(delta):
	self.rotation.y = lerp_angle(self.rotation.y , self.rotationAngle, delta)
	var velocity = Vector3(self.positionX - self.global_transform.origin.x + self.offsetX, 0, self.positionY - self.global_transform.origin.z + self.offsetY)
	velocity = move_and_slide(velocity)

func set_socket(socket):
	_socket = socket
	
	
func connect_to_robot(ip, port, path=""):
	if port.length() == 0:
		port = 9998
	_socket = WebSocketClient.new()
	var progress = load("res://objects/gui/LoadingView.tscn").instance();
	progress.setLabel("Connecting to the Robot...");
	get_tree().get_root().get_node("World/GUI").add_child(progress)
	# Connect base signals to get notified of connection open, close, and errors.
	_socket.connect("connection_closed", self, "_closed")
	_socket.connect("connection_error", self, "_closed_error")
	_socket.connect("connection_established", self, "_connected")
	# This signal is emitted when not using the Multiplayer API every time
	# a full packet is received.
	# Alternatively, you could check get_peer(1).get_available_packets() in a loop.
	_socket.connect("data_received", self, "_on_data")

	# Initiate connection to the given URL.
	var url
	if port:
		url = "ws://"+ip+":"+str(port)+path
	else :
		url = "ws://"+ip+path
	
	print("Connecting to ", url)
	
	var err = _socket.connect_to_url(url)
	
	if err != OK:
		
		print("Unable to connect")
		progress.queue_free()
		var errorDialog = load("res://objects/gui/ErrorView.tscn").instance()
		errorDialog.get_node("PanelContainer/VBoxContainer/RichTextLabel").text="Connection error!"
		get_tree().get_root().get_node("World/GUI").add_child(errorDialog)
		queue_free()


func _closed(was_clean = false):
	# was_clean will tell you if the disconnection was correctly notified
	# by the remote peer before closing the socket.
	print("Closed, clean: ", was_clean)
	if !was_clean:
		queue_free()
	
	set_process(false)


func _closed_error(was_clean = false):
	var progress = get_tree().get_root().get_node("World/GUI/LoadingView")
	if progress:
		progress.queue_free()
	var errorDialog = load("res://objects/gui/ErrorView.tscn").instance()
	errorDialog.get_node("PanelContainer/VBoxContainer/RichTextLabel").text="Connection error!"
	get_tree().get_root().get_node("World/GUI").add_child(errorDialog)
	set_process(false)
	queue_free()
	
func _connected(proto = ""):
	var progress = get_tree().get_root().get_node("World/GUI/LoadingView")
	if progress:
		progress.queue_free()
	# This is called on connection, "proto" will be the selected WebSocket
	# sub-protocol (which is optional)
	print("Connected with protocol: ", proto)
	# You MUST always use get_peer(1).put_packet to send data to server,
	# and not put_packet directly when not using the MultiplayerAPI.
	_socket.get_peer(1).put_packet("Test packet".to_utf8())


		
func _on_data():
	# Print the received packet, you MUST always use get_peer(1).get_packet
	# to receive data from server, and not get_packet directly when not
	# using the MultiplayerAPI.
	#print("Got data from server: ", _socket.get_peer(1).get_packet().get_string_from_utf8())
	var jsonResult = JSON.parse(_socket.get_peer(1).get_packet().get_string_from_utf8())
	if jsonResult.error == OK :
		var jsonData = jsonResult.result
		#print(jsonData["command"])
		if  jsonData["command"] == "position":
			
			
			self.rotationAngle = deg2rad(jsonData["data"]["Angle"] ) 
			if self.positionX == null and self.positionY == null:
				self.global_transform.origin.x = jsonData["data"]["X"]/scalePosizion
				self.global_transform.origin.z = -jsonData["data"]["Y"]/scalePosizion
			self.positionX = jsonData["data"]["X"]/scalePosizion
			self.positionY = -jsonData["data"]["Y"]/scalePosizion
			if self.positionX > 0 and self.positionY > 0:
				meshSmall.disabled = true
				timerCollision.start()
			$Coordinates.update_info(jsonData["data"]["X"], jsonData["data"]["Y"] , jsonData["data"]["Angle"] )
		elif jsonData["command"] == "battery":
			print("Aggiorno batteria");
			batteryBar.set_bar_value(int(jsonData["data"]["percent"]))
		#print(jsonData)
	

func _on_CollisionEnablerTimer_timeout():
	meshSmall.disabled = false


func _on_ClickArea_mouse_entered():
	outline.visible = true


func _on_ClickArea_mouse_exited():
	outline.visible = false
