extends Spatial




# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func update_info(x, y ,a):
	$Viewport/PanelContainer/VBoxContainer/HBoxContainer/x_coord.text = "X: " + str(x)
	$Viewport/PanelContainer/VBoxContainer/HBoxContainer/y_coord.text = "Y: " + str(y)
	$Viewport/PanelContainer/VBoxContainer/HBoxContainer/angle.text = "Angle: " + str(a) + "°"
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
