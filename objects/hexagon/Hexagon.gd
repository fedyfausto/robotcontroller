extends Spatial
tool

onready var mesh = $RigidBody/MeshInstance
export(String, "Blue", "Green", "Red") var color 

func _ready():
	var material = self.get_node("RigidBody/MeshInstance").get_surface_material(0)
	match color:
		"Blue":
			material.albedo_color = Color.blue
		"Green":
			material.albedo_color = Color.green
		"Red":
			material.albedo_color = Color.red
	self.get_node("RigidBody/MeshInstance").set_surface_material(0, material)

func _process(delta):
	var material = self.get_node("RigidBody/MeshInstance").get_surface_material(0)
	match color:
		"Blue":
			material.albedo_color = Color.blue
		"Green":
			material.albedo_color = Color.green
		"Red":
			material.albedo_color = Color.red
	self.get_node("RigidBody/MeshInstance").set_surface_material(0, material)

