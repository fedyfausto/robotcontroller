extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


export(NodePath) var robotNode
export (Color, RGB) var colorNo
export (Color, RGB) var colorOk

onready var camera = get_parent().get_node("Camera")
onready var raycast = $RayCast
onready var mesh = $MeshInstance

var can_place = true
var placing = false

var robot
var point
var object_to_place = null
var object_placing = null

# Called when the node enters the scene tree for the first time.
func _ready():
	
	robot = get_node_or_null(robotNode)
	pass # Replace with function body.



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
#	if placing:
#		mesh.visible = true
##		#Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
##		#Input.set_custom_mouse_cursor(null)
#	else:
#		mesh.visible = false
		#Input.set_custom_mouse_cursor(null)
		#Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	#LineDrawer.DrawLine(self.global_transform.origin, robot.global_transform.origin, Color(0, 0, 1), 1.5)
	pass

func _input(event):
	if event is InputEventMouseButton and event.pressed:
		#var pg = get_parent().get_node("Objects/Knight")
		#pg.target = point
		
		if event.button_index == BUTTON_LEFT and event.pressed and object_placing!= null and can_place:
			var obj = object_placing.instance()
			get_tree().get_current_scene().get_node("Objects").add_child(obj)
			point.y += (object_to_place.scale.y * .05) + 0.1
			obj.set_translation(point)
			(object_to_place as RigidBody).queue_free()
			get_node("Area/CollisionShape").queue_free()
			object_placing = null
			can_place = false
			placing = false
			mesh.visible = true
		
	pass

func _physics_process(delta):
	_updateMousePosition()
	pass

func _updateMousePosition():
		
	var mouse = get_viewport().get_mouse_position()
	var from = camera.project_ray_origin(mouse)
	var to = from + camera.project_ray_normal(mouse) * 2000
		
	var space_state = get_world().get_direct_space_state()
	var hit = space_state.intersect_ray(from, to, [robot])
	if hit.size() != 0:
		point = hit.position
		self.set_translation(hit.position)
	
	pass



func _on_Area_body_entered(body):
	if placing and object_to_place != null:
		object_to_place.get_node("MeshInstance").get_surface_material(0).set_shader_param('current_color', colorNo)
		can_place = false
	pass # Replace with function body.


func _on_Area_body_exited(body):
	if placing and object_to_place != null:
		object_to_place.get_node("MeshInstance").get_surface_material(0).set_shader_param('current_color', colorOk)
		can_place = true
	pass # Replace with function body.


func _on_GUIController_new_object_place(object):
	if !placing:
		mesh.visible = false
		var material = load("res://assets/materials/mouse_material.tres")
		object_to_place = object.instance()
		object_to_place.collision_mask = 0
		object_to_place.collision_layer = 0
		object_to_place.sleeping = true
		object_to_place.name="ObjectToPlace"
		object_to_place.get_node("MeshInstance").set_surface_material(0, material)
		
		self.add_child(object_to_place)
		
		var newCollision = get_node("ObjectToPlace/CollisionShape").duplicate()
		#newCollision.scale = object_to_place.scale
		var trans = newCollision.translation
		trans.y+= newCollision.scale.y
		newCollision.set_translation(trans)
		self.get_node("Area").add_child(newCollision)
		object_to_place.get_node("MeshInstance").get_surface_material(0).set_shader_param('current_color', colorOk)
		object_placing = object
		can_place = true
		placing = true 


